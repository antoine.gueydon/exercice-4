package fr.cnam.foad.nfa035.dao;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;

public class BadgeWalletDAO {
    File badge;

    /**
     * Constructeur permettant d'instancier des objets de type BadgeWalletDAO, à partir d'une adresse de fichier s
     * @param s
     */
    public BadgeWalletDAO(String s) {

        this.badge = new File(s);

    }

    /**
     * Cette classe sérialise une image et place le résultat dans l'objet BadgeWalletDAO courrant.
     * @param image
     */
    public void addBadge(File image) {
        try {
            ImageFileFrame wallet = new ImageFileFrame(this.badge);
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(image, wallet);

        }
        catch(Exception e)
        {
            e.printStackTrace();

        }

    }
}
