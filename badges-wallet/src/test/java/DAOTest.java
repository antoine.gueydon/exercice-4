import fr.cnam.foad.nfa035.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.test.SimpleAccessTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class DAOTest {

    private static final Logger LOG = LogManager.getLogger(DAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    /**
     * Cette méthode initialise un fichier walletDatabase
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (walletDatabase.exists()){
            walletDatabase.delete();
            walletDatabase.createNewFile();
        }
    }

    /**
     * cette méthode ajoute un nouveau badge à partir d'une image, vers un fichier. Ensuite elle teste si ce que contient le fichier correspond à l'image sérialisée
     */
    @Test
    public void testAddBadge(){
        try {

            File image = new File(RESOURCES_PATH + "petite_image.png");
            BadgeWalletDAO dao = new BadgeWalletDAO(RESOURCES_PATH + "wallet.csv");
            dao.addBadge(image);

            String serializedImage = new String(Files.readAllBytes(walletDatabase.toPath()));
            LOG.info("Le badge-wallet contient à présent cette image sérialisée:\n{}", serializedImage);

            // Utilisation des outils pour comparer avec le résultat attendu
            ImageSerializer serializer = new ImageSerializerBase64Impl();
            String encodedImage = (String) serializer.serialize(image);

            // astuce pour ignorer les différences de formatage entre outils de sérialisation base64:
            serializedImage = serializedImage.replaceAll("\n","").replaceAll("\r","") ;

            assertEquals(serializedImage, encodedImage);

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }

}
